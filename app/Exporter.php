<?php
 abstract class Exporter implements Exportable,Importable{
     protected $data ;
     protected static $extention ;
     protected static $path_saved_file ;
     protected $file_name ;
     /**
      * Class constructor.
      */
     public function __construct(string $file_name)
     {
         $this->file_name = $file_name;
     }
    /**
     * set data .
     */
     public function import(string $data)
     {
        $this->data = $data ;
     }
     /**
      * getdata
      */
      public function getData(){
          return $this->data ;
      }

 }