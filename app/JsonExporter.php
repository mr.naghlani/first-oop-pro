<?php

class JsonExporter extends Exporter 
{
    protected static $extention = 'json';
    protected static $path_saved_file = 'upload/Json/';

    public function export()
    {
        $file_path_saved = static::$path_saved_file . $this->file_name . '.' . static::$extention;
        $url = BASE_URL .  $file_path_saved;
        $data = [
            'file_name' => $this->file_name,
            'cretae_at' => date('Y-m-d h:i:s'),
            'data'      => $this->getData(),
        ];
        $json_format = json_encode($data);
        file_put_contents($file_path_saved, $json_format);
        echo '<a href="' . $url . '">Download File </a>';   
    }
}