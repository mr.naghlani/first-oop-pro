<?php
class TxtExporter extends Exporter {

    protected static $extention = 'txt';
    protected static $path_saved_file = 'upload/Txt/';
      
      public function export(){
        $file_path_saved = static::$path_saved_file . $this->file_name . '.' . static::$extention;
        $url = BASE_URL .  $file_path_saved  ;    
        file_put_contents($file_path_saved, $this->getData());
        echo '<a href="' . $url . '">Download File </a>' ;
      }
    
}