<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

use \Mpdf\Mpdf;

class PdfExporter extends Exporter
{
    protected static $extention = 'pdf';
    protected static $path_saved_file = 'upload/Pdf/';

    public function export()
    {
        $file_path_saved = static::$path_saved_file . $this->file_name . '.' . static::$extention;
        $pdfGnerator = new mPDF();
        $pdfGnerator->allow_charset_conversion = true;
        $pdfGnerator->charset_in = 'utf-8';
        $pdfGnerator->SetHTMLHeader('
            <div style="text-align: right; font-weight: bold;">
                My document
            </div>');
        $pdfGnerator->SetHTMLFooter('
            <table width="100%">
                <tr>
                    <td width="33%">{DATE j-m-Y}</td>
                    <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                    <td width="33%" style="text-align: right;">My document</td>
                </tr>
            </table>');
        $pdfGnerator->WriteHTML(utf8_encode($this->getData()));
        $pdfGnerator->Output($file_path_saved);
    }
}
