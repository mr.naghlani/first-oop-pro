<?php
class ImgExporter extends Exporter
{
    protected static $extention = 'jpg';
    protected static $path_saved_file = 'upload/Jpg/';

    public function export()
    {
        $img = ImageCreateTrueColor(400 , 40);
        $bg  = imagecolorallocate($img , 255,255,255);
        imageFill($img , 0 , 0 , $bg);
        $text_color = imagecolorallocate($img , 0 , 0 , 0);
        imagestring($img, 4, 0, 0, $this->getData(), $text_color);
        $url = static::$path_saved_file . $this->file_name . '.' . static::$extention;
        imageJPEG($img , $url );
        echo '<img src="' . BASE_URL . $url . '" style=" border:1px solid #000000">'; 
    }
}