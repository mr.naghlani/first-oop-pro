<?php
function autoload($class_name)
{
    $class_name = trim($class_name);
    $file_path = BASE_PATH .  'app/' .$class_name . '.php';
    if(file_exists($file_path)){
        require_once ($file_path) ;
    }else{
        die('Class Name Not Found !') ;
    }
}

spl_autoload_register("autoload");