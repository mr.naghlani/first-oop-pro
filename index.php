<?php require_once ('init.php') ;?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <title>form</title>
</head>

<body>
    <div class="container m-4">
        <div class="row justify-content-center">
            <dir class="col-6 border border-warning p-2 rounded-sm shadow-sm">
                <form action="exportGnarator.php" method="POST">
                    <div class="input-group mt-2">
                        <input type="text" name="fileName" class="form-control" placeholder="Select file name without extention ...">
                    </div>
                    <div class="input-group mt-2">
                        <select name="extention" class="form-control">
                            <option value="pdf">Pdf</option>
                            <option value="txt">Txt</option>
                            <option value="json">Json</option>
                            <option value="csv">Csv</option>
                            <option value="img">img</option>
                        </select>
                    </div>
                    <div class="input-group mt-2">
                        <textarea name="data" class="form-control" rows="6" placeholder="Insert Data ... "></textarea>
                    </div>
                    <div class="input-group mt-2">
                        <input type="submit" name="submit" value="submit" class="form-control btn btn-success">
                    </div>
                </form>
            </dir>
        </div>
    </div>

</body>

</html>