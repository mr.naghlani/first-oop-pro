<?php
require_once ('init.php');
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $file_name = trim($_POST['fileName']);
    $ext = trim($_POST['extention']);
    $data = trim($_POST['data']);
    // use class 
    $class_name = ucfirst($ext) . 'Exporter' ;
    $export = new $class_name ($file_name);
    $export->import($data);
    $export->export();
}
